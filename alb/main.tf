module "alb" {
  source = "github.com/jonzxz/jonnywritesterraform/loadbalancer"
  # source = "../../jonnywritesterraform/loadbalancer"
  vpc_name = var.vpc_name
  name     = var.alb_name
}