variable "alb_name" {
  type        = string
  description = "Name of application load balancer"
  default     = "vend-ip-alb"
}

variable "vpc_name" {
  type        = string
  description = "Name of VPC"
  default     = "vend-ip-vpc"
}