# VEND-IP Assignment

## Repository Structure
1. `vpc`
    - Creates a VPC that comes bundled with 2 sets of public and private subnets, NAT, IGW.
    - Source: https://github.com/jonzxz/jonnywritesterraform/tree/master/vpc
1. `alb`
    - Creates an application load balancer and it's relevant security group for HTTP 80 access.
    - Source: https://github.com/jonzxz/jonnywritesterraform/tree/master/loadbalancer
1. `nginx`
    - Creates an autoscaling group that spans across 2 AZs in a private subnet.
    - Bootstrapped with some boiler code to set up nginx in a really, really hacky way. More info later on.
    - Source: https://github.com/jonzxz/jonnywritesterraform/tree/master/autoscaling

## Design (and flaws!)

### Terraform Structure
1. Each components are compartmentalized into each subdirectory, where each of them has their own state.
    1. Ideally speaking, they should really be in a single root module so I can make use of the module outputs to feed into other modules
    1. And doing it this way creates direct dependencies that are not explicit, so it can be cumbersome in trying to figure out what pre-requisites are missing..
1. All of the modules used in this assignment are code I have written myself (either as practice or just for my own sandboxing).
    1. Some of them are **really terrible** with a lot of inherent design flaws that I did not consider when I wrote them initially.
    1. And the modules used here are not entirely flexible due to it's nature of being bounded to be used for NGINX (particularly the ALB and ASG)
1. The reason why it's done this way will be elaborated below.. but mostly due to my *lack of experience when I wrote the reusable modules a long time ago*.

### VPC Design
1. The VPC is created with some bundled components
    1. VPC
    1. 2x Private subnets across 2 AZs
    1. 2x Public subnets across 2 AZs
    1. 2x NAT/EIP in each public subnet created
    1. 1x IGW
    1. Routing configurations (rules, tables, etc)

### ALB Design
1. The ALB module creates the following
    1. **Application**  Load Balancer across 2 public subnets
    1. LB Listener for HTTP **only**
    1. Security Group for inbound HTTP **only**
1. There are some inherent issues with the design of the module itself -
    1. The LB listener and SG only allows port 80 and is non configurable
    1. The way the VPC ID and subnet IDs are fed into the LB and security groups are done horrendously..
        1. The base module uses `data` blocks to retrieve VPC and subnet (via VPC and subnet name filter) via `vpc_name`, which in itself is honestly a very bad idea.
        1. The module should just ideally receive a `vpc_id` and `[subnet_ids]` so that I can pass them in from the `vpc` module's outputs..

### Nginx Autoscaling Design
1. The nginx module creates the following
    1. Autoscaling group along with launch templates
    1. An IAM role and instance profile which takes in configurable policies with a custom merge logic
    1. A security group that **only** allows port 80 ingress from the ALB security group
        1. Which by itself is a terrible idea, because this creates an implicit dependency of the ALB's SG.
    1. A security group rule that creates an egress of port 80 **on the ALB SG** 
        1. I have no idea why I did this in the past, but it is a terrible idea.
1. It has a `bootstrap.sh` that runs **really** hacky stuff..
    1. I actually smacked the `vend-ip` nginx configuration on instance runtime.. Alternatively it could probably have been from S3 or file provisioner or a better way to do it completely.

## Assignment Requirements
1. CIDR retrieve REST API from `vend_ip` endpoint
    1. I used IMDSv2 (which is enforced on LT level) to retrieve the instance IP and subnet mask.
    1. The data is populated when the instance is bootstrapped via a combination of curl and jq.
1. Create subnets with size /24
    1. There are a total of 4 subnets, 2 public and 2 privates. All of them are /24 based on the `cidrsubnet` logic in place.
1. Generate SSH key for VM credentials
    1. **I didn't do this.** I personally think SSH isn't a very good idea in this case - given that my instances are launched in the private subnets, even with a SSH key I'll need a bastion to connect to them.
    1. I opted for SSM instead (hence the HTTPS 443 outbound for the autoscaling security group)
1. Take into consideration CSP Best Practices
    1. Admittedly the NACL is not the best idea since it has allow in/out on all, but I restricted the SG access on ALB and the ASG.
    1. IAM roles are also reduced to what's only needed (SSM and CWL)
    1. I exposed HTTPS 443 outbound for the SSM (and access to package managers)
1. Take into consideration coding/scripting practices
    1. I'm not entirely sure what is required here, but I tried to "make neat" the bash script with shebangs and variables, although I think `sudo` in the bootstrap isn't a very good idea.
1. Leverage on native cloud metrics/logging for error handling
    1. I installed `awslogsd` which is the Cloudwatch Agent for Amazon Linux 2, but **I did not configure the CW Agent files to flow Nginx error logs**.
    1. Admittedly I am not very familiar with logging..
1. Tech stack and CICD
    1. I used Terraform for the infrastructure with some bash for bootstrapping, and CI is in GitLab.

## Proof of Work
- Working [pipeline](https://gitlab.com/jonathan.kerk/vend-ip/-/pipelines/1006639971)

- Screenshot

![my terrible work..](./alb.jpg)

## Improvements
> This is going to be a long section, I know my base modules are terrible, please forgive me..

1. As mentioned, the root module should be in a single state rather than separated since this creates implicit dependencies that are not anywhere mentioned.
1. Usage of `data` blocks to retrieve VPC IDs and Subnet IDs are not the best idea in the reusable modules. If the modules consume `id` as variables and the root module is compacted then I can simply use outputs rather than this monstrous `data` blocks.
1. The reusable modules are not robust enough - the ALB and autoscaling modules are really for nginx and nginx only. It doesn't work with anything else unless your other web servers are hosted on port 80.
1. The bootstrap is way too hacky - I'm sure there's another better way to configure nginx, but I am not absolutely familiar with configuring one from scratch.
1. Cloudwatch Agent configurations..
1. The CICD are using public runners and I smacked my AWS access keys into the environment variables. Ideally this should've been done with a STS assume role with properly scoped permissions..
1. I should probably add `rm -r .terraform` to prevent any caches..

## Final Words
1. Thank you for taking your time to reach the end of this documentation (and reviewing the code!)
1. My sincere apologies for not completing all of the requirements.
1. I am terribly sorry for my past self for writing such terrible Terraform code.
    1. I decided to use the modules I wrote in the past instead of rewriting new code to attest whether they are robust enough, but apparently not (sad).
1. I hope whoever that is reviewing this assignment is not disgusted at my code, I write better code now (I think)!
1. The resources created here are already all destroyed, so the LB DNS name is not accessible.
