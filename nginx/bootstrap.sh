#!/bin/bash
sudo amazon-linux-extras enable epel
sudo yum install epel-release jq awslogs -y
sudo amazon-linux-extras install nginx1 -y

sudo systemctl start awslogsd

TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`

IP_ADDR=$(curl --silent -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/local-ipv4)
INTERFACE=$(curl --silent -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/mac)
SUBNET_MASK=$(curl --silent -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/network/interfaces/macs/$INTERFACE/subnet-ipv4-cidr-block | cut -d '/' -f 2)

VEND_IP_HTML_FILE_PATH="/usr/share/nginx/html/vend-ip/"
VEND_IP_CONF_FILE_PATH="/etc/nginx/conf.d/vend-ip.conf"

echo -e '{ "ip_address" : "'${IP_ADDR}'", "subnet_size" : "/'${SUBNET_MASK}'" }' | jq '.' > /tmp/index.html

sudo mkdir -p ${VEND_IP_HTML_FILE_PATH}
sudo mv /tmp/index.html ${VEND_IP_HTML_FILE_PATH}

sudo echo -e "server {\n\tlisten 80;\n\tlocation /vend-ip/ {\n\t}\n}" > /tmp/vend.conf
sudo mv /tmp/vend.conf ${VEND_IP_CONF_FILE_PATH}

sudo systemctl start nginx
