module "nginx_autoscaling" {
  source = "github.com/jonzxz/jonnywritesterraform/autoscaling"
  # source              = "../../jonnywritesterraform/autoscaling"
  service_name        = "vend-ip"
  vpc_name            = "vend-ip-vpc"
  bootstrap_file_name = filebase64("bootstrap.sh")
  alb_name            = "vend-ip-alb"

  custom_policies = [data.aws_iam_policy_document.cloudwatch.json]
}

data "aws_iam_policy_document" "cloudwatch" {
  statement {
    sid = "AllowPublishCloudWatch"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]
    effect = "Allow"
    resources = [
      "*"
    ]
  }
}
