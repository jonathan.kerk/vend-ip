terraform {

  required_version = ">= 1.5.0"

  backend "s3" {
    bucket = "jonny-multi-purpose-bucket"
    key    = "terraform/playground/vend-ip-nginx/terraform.tfstate"
    region = "ap-southeast-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.16.2"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"

  default_tags {
    tags = {
      "Contact" : "contact@jonathankerk.com"
    }
  }
}