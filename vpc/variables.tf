variable "vpc_cidr_block" {
  type        = string
  description = "VPC CIDR Range"
  default     = "172.31.0.0/22"
}

variable "vpc_name" {
  type        = string
  description = "Name of VPC"
  default     = "vend-ip-vpc"
}
