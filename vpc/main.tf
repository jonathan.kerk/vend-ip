module "vpc" {
  source = "github.com/jonzxz/jonnywritesterraform/vpc"
  # source     = "../../jonnywritesterraform/vpc"
  vpc_name   = var.vpc_name
  cidr_block = var.vpc_cidr_block
}